Por lo pronto sólo colocaré archivos relacionados con el corpus.
El corpus completo en XML no se publicará. En su lugar, publicaré en una versión que posibilite su libre
intercambio, pero aún estoy trabajando en ello.
El corpus tiene finalidad de investigación científica, por lo que si se desea
una copia, por favor contactarme para mencionarles qué procedería ya que
es necesario un documento de "colaboración en investigación".