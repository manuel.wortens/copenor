from random import randint
import pandas as pd
import numpy as np
import openpyxl as ox


df = pd.read_excel (r'COPENOR_MEDIOS.xlsx')
num_medios = len(df.index)+1
total_acumulativo = df.groupby(['Estado']).count().cumsum()
cnt = 0
df_master = pd.DataFrame()

while cnt < 380:
    value = np.random.randint(1,num_medios)
    n = 0
    z = 1
    while n < len(total_acumulativo.index):
        y = total_acumulativo.iloc[n][0]+1
        if value in range(z,y):
            estado_loc = ''.join(total_acumulativo.index[total_acumulativo['ID'] == y-1].tolist())
            df_master = pd.concat([df_master,df.set_index(['Estado']).loc[estado_loc].sample(1)])
        n = n+1
        z = total_acumulativo.iloc[n-1][0]+1
    cnt = cnt+1
df_master.to_excel("itinerario.xlsx",startcol=2)

#itinerario de trabajo

m = 2
book = ox.load_workbook('itinerario.xlsx')
sheet = book.active
sheet.cell(row=1, column=1).value = "Fecha Captura"
datelist = pd.date_range(pd.datetime.today(), periods=55).tolist()
for x in datelist:
    sheet.cell(row=m, column=1).value = str(x).split()[0]
    m = m+7
book.save('itinerario.xlsx')
